# Crud Interns Api

An adonis application that exposes CRUD functionalities for interns

## Swagger URl

[Swagger URL](https://dorcastoto.github.io/CRUD-adonis/)
## user requirements
 - Create interns resource endpoints that expose crud functionalities for interns.
  - The data should be stored in a database that you will configure.
  -  The intern objects being mocked should contain, at least, the following properties.
    - Name of the intern
    - Email of the intern
    - An array of the tech stack the user shall build an app with

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
