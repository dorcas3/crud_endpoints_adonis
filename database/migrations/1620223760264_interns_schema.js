'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InternsSchema extends Schema {
  up () {
    this.create('interns', (table) => {
      table.increments()
      table.string('fullnames', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.integer('age').notNullable()
      table.json('stacks').notNullable()

      table.timestamps()
    })
  }

  down () {
    this.drop('interns')
  }
}

module.exports = InternsSchema
