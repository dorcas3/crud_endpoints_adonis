'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with interns
 */
const Intern = use('App/Models/Intern')
class InternController {
  /**
   * Show a list of all interns.
   * GET interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async index ({ request, response, view }) {
    let interns = await Intern.all()
    return response.send(interns)
  }

  /**
   * Render a form to be used for creating a new intern.
   * GET interns/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   * 
   */

  async create ({ request, response, view }) {

  }

  /**
   * Create/save a new intern.
   * POST interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {

    const { fullnames, email, age, stacks } = request.only(['fullnames', 'email','age', 'stacks'])
    const stackArr = JSON.stringify(stacks)
    let intern = new Intern()
    intern.fullnames = fullnames
    intern.email = email
    intern.age = age
    intern.stacks = stackArr
    await intern.save()
    response.ok(intern)

    // let data = request.all()
    // let intern = await Intern.create(data)

    // return response.send(intern)


  }

  /**
   * Display a single intern.
   * GET interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const intern = await Intern.find(params.id)
    return response.status(200).send(intern)
  }

  /**
   * Render a form to update an existing intern.
   * GET interns/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update intern details.
   * PUT or PATCH interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const data = request.only(['fullnames', 'email','age', 'stacks'])
    const intern = await Intern.find(params.id)
    if (!intern){
      return response.status(404).json({
        data:'Resource not found'
      })
  }

  intern.fullnames = data.fullnames
  intern.email = data.email
  intern.age = data.age
  intern.stacks = JSON.stringify(data.stacks)

  await intern.save()
  
  return response.status(200).json(intern)

  
  }
  /**
   * Delete a intern with id.
   * DELETE interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const intern = await Intern.find(params.id)
    if (!intern){
      return response.status(404).json({
        data:'Resource not found'
      })
     
    }
    await intern.delete()
    return response.status(204).json(null)
  }
}

module.exports = InternController
